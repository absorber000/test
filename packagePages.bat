@Echo Off

chcp 1251 >nul

Set "BoxIn=E:\work\sm-landing\dist"
Set "MaskIn=*.html"

FOR /F "usebackq delims=" %%f IN (`Dir "%BoxIn%\%MaskIn%" /B /A:-D 2^>nul`) DO (
	FOR /F "tokens=1 delims=_" %%i IN ("%%f") DO (
        del /q E:\work\sm-landing\dist-min\*
        MD E:\work\sm-landing\dist-min\%%~ni
		Copy "%BoxIn%\%%f" "dist-min\%%~ni\index.html" >nul
        @RD /S /Q E:\work\sm-landing\dist-min\404
        @RD /S /Q E:\work\sm-landing\dist-min\index
        Copy "E:\work\sm-landing\dist\404.html" "E:\work\sm-landing\dist-min\" >nul
        Copy "E:\work\sm-landing\dist\index.html" "E:\work\sm-landing\dist-min\" >nul
        Copy "E:\work\sm-landing\app\robots.txt" "E:\work\sm-landing\dist-min\" >nul
        Copy "E:\work\sm-landing\app\sitemap.xml" "E:\work\sm-landing\dist-min\" >nul
        robocopy "E:\work\sm-landing\app\mailSend" "E:\work\sm-landing\dist-min\mailSend" /s /e /XD
        robocopy "E:\work\sm-landing\app\helpers" "E:\work\sm-landing\dist-min\helpers" /s /e /XD
        robocopy "E:\work\sm-landing\dist\css" "E:\work\sm-landing\dist-min\css" /s /e /XD
        robocopy "E:\work\sm-landing\dist\js" "E:\work\sm-landing\dist-min\js" /s /e /XD
        robocopy "E:\work\sm-landing\dist\images" "E:\work\sm-landing\dist-min\images" /s /e /XD
        robocopy "E:\work\sm-landing\app\fonts" "E:\work\sm-landing\dist-min\fonts" /s /e /XD
        Copy "E:\work\sm-landing\app\js\YaMap.js" "E:\work\sm-landing\dist-min\js" >nul
	)
)