"use strict";

;(function (win, doc) {
    'use strict';

    var $html = $("html");
    var $body = $("body");

    function isElementInView(elem, isOnlyFullObject) {
        var isInView = false;
        var $elem = $(elem);

        if (isOnlyFullObject === undefined) {
            isOnlyFullObject = false;
        }

        if ($elem.is(':visible') && $elem.height() > 0) {
            var $window = $(window);

            // Проверяем попадает ли элемент в экран по высоте
            var windowScrollTop = $window.scrollTop();
            var windowBottomOffset = windowScrollTop + $window.height();
            var elemTopOffset = $elem.offset().top;
            var elemBottomOffset = elemTopOffset + $elem.innerHeight();

            if (isOnlyFullObject) {
                isInView = windowScrollTop <= elemTopOffset && windowScrollTop < elemBottomOffset && windowBottomOffset >= elemBottomOffset && windowBottomOffset > elemTopOffset;
            } else {
                isInView = windowBottomOffset > elemTopOffset && windowScrollTop < elemBottomOffset;
            }

            if (isInView) {
                // Проверяем попадает ли элемент в экран по ширине
                var windowScrollLeft = $window.scrollLeft();
                var windowRightOffset = windowScrollLeft + $window.width();
                var elemLeftOffset = $elem.offset().left;
                var elemRightOffset = elemLeftOffset + $elem.innerWidth();

                if (isOnlyFullObject) {
                    isInView = windowScrollLeft <= elemLeftOffset && windowScrollLeft < elemRightOffset && windowRightOffset >= elemRightOffset && windowRightOffset > elemLeftOffset;
                } else {
                    isInView = windowRightOffset > elemLeftOffset && windowScrollLeft < elemRightOffset;
                }
            }
        }

        return isInView;
    };
    var Modal = machina.Fsm.extend({
        initialize: function initialize(el, selector, color, content, callbackOpen, callbackClose) {
            this.callbackOpen = el.callbackOpen;
            this.callbackClose = el.callbackClose;
            this.$elId = el.el;
            this.$el = $(this.$elId);
            this.$sel = $(this.selector);
            this.modalDialog = this.$el.find("#modal-dialog");
            this.$overlay = this.$el.find("#modal-overlay");
            this.$modalTxt = this.$el.find("#modal-txt");
            this.$closeBtn = this.$el.find("#modal-callback-close");
            this.options = el.options;
            this.init();
        },
        namespace: "modal",
        initialState: "uninitialized",
        states: {
            uninitialized: {
                _onEnter: function _onEnter() {
                    this.handle("toClose");
                },
                toClose: function toClose() {
                    this.transition("close");
                }
            },
            close: {
                _onEnter: function _onEnter() {
                    this.close();
                }
            },
            open: {
                _onEnter: function _onEnter() {
                    this.open();
                }
            }
        },
        callModal: function callModal() {
            var context = this;
            this.$sel.click(function () {
                if (context.state === "close") {
                    context.$elFormDesc = $(this).attr('data-modal-form-desc');
                    context.$elTitle = $(this).attr('data-modal-title');
                    context.$elBtnText = $(this).attr('data-modal-btn-text');
                    context.transition("open");
                    context.callbackOpen();
                }
                context.modalDialog.css({ "background-color": context.color });
                context.$modalTxt.text(context.content);
            });
        },
        overlayClick: function overlayClick() {
            var context = this;
            this.$overlay.click(function () {
                if (context.state === "open") {
                    context.transition("close");
                    context.callbackClose();
                }
            });
        },
        closeClick: function closeClick() {
            var context = this;
            this.$closeBtn.click(function () {
                if (context.state === "open") {
                    context.transition("close");
                    context.callbackClose();
                }
            });
        },
        addEvents: function addEvents() {
            this.callModal();
            this.overlayClick();
            this.closeClick();
        },
        open: function open() {
            var _this = this;

            var scrollbarWidth = $(document).width() - window.innerWidth;
            this.$el.stop().fadeIn(200, function () {
                _this.$el.addClass("modal--active");
            });
            $("html").addClass("modal-page");
            $("body").css({ "padding-right": -scrollbarWidth });
        },
        close: function close() {
            this.$el.removeClass("modal--active");
            this.$el.stop().fadeOut(200, function () {
                $("html").removeClass("modal-page");
                $("body").css({ "padding-right": "" });
            });
        },
        init: function init() {
            this.addEvents();
        }
    });
    var stickyNavbar = {
        target: doc.getElementById("header-navbar"),
        dummy: doc.getElementById("header-navbar-dummy"),
        stucked: false,
        init: function init() {
            var _this2 = this;

            win.addEventListener('scroll', function (event) {
                return _this2.stuck();
            });
            this.stuck();
        },


        stuck: function stuck() {
            //console.log(this.target.getBoundingClientRect().top);

            if (this.dummy.getBoundingClientRect().top < 0 && !this.stucked) {
                this.stucked = true;
                //console.log(this.stucked);
                //console.log("1");
                this.target.classList.add("header-navbar--stucked");
            }
            if (this.dummy.getBoundingClientRect().top > 0 && this.stucked) {
                this.stucked = false;
                this.target.classList.remove("header-navbar--stucked");
            }
        }
    };
    var mobileMenu = {
        $burger: $("#burger"),
        $menu: $("#menu"),
        menuActive: false,
        $menuOverlay: $("#menu-overlay"),
        openMenu: function openMenu() {
            var _this3 = this;

            this.$menu.addClass("menu--active");
            this.menuActive = true;
            this.$menuOverlay.stop().fadeIn(400, function () {
                _this3.$menuOverlay.addClass("menu-overlay--active");
            });
        },

        closeMenu: function closeMenu() {
            var _this4 = this;

            this.$menu.removeClass("menu--active");
            this.menuActive = false;
            this.$menuOverlay.stop().fadeOut(400, function () {
                _this4.$menuOverlay.removeClass("menu-overlay--active");
            });
        },

        init: function init() {
            var _this5 = this;

            $(".menu__link.js-scrollScreen").click(function () {
                if (_this5.menuActive) {
                    _this5.closeMenu();
                }
            });
            $("#icon-close").click(function () {
                if (_this5.menuActive) {
                    _this5.closeMenu();
                }
            });
            this.$burger.click(function () {
                if (_this5.menuActive) {
                    _this5.closeMenu();
                } else if (!_this5.menuActive) {
                    _this5.openMenu();
                }
            });
            this.$menuOverlay.click(function () {
                if (_this5.menuActive) {
                    _this5.closeMenu();
                }
            });
        }
    };
    function modalInit() {
        function _callbackOpen() {
            // if (iOS() && isTouch){
            //     if (!$body.hasClass("body--fix")){
            //         $body.addClass("body--fix");
            //     }
            // }
            // if (win.innerWidth <= 767){
            //     doc.querySelector("#page-header").classList.add("page-header--abs");
            // }
        }
        function _callbackClose() {
            // $("#callback-form").find(".hidden-form_desc").val("");
            // if ($body.hasClass("body--fix")){
            //     $body.removeClass("body--fix");
            // }
            // if (win.innerWidth <= 767){
            //     doc.querySelector("#page-header").classList.remove("page-header--abs");
            // }
            // if ($body.hasClass("modal-form-success")){
            //     $("#modal-callback .form-in").css({"display" : "block"});
            //     $("#modal-callback .fin-mess").css({"display" : "none"});
            //     $("#modal-callback .modal-callback__input-block input").val("");
            // }
        }
        var modal1 = new Modal({
            el: "#modal",
            selector: ".js-modal1",
            color: "#4cb1ca",
            content: "Идейные соображения высшего порядка, а также сложившаяся структура организации обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также сложившаяся структура организации требуют определения и уточнения системы обучения кадров, соответствует насущным потребностям. Равным образом сложившаяся структура организации влечет за собой процесс внедрения и модернизации позиций, занимаемых участниками в отношении поставленных задач.",
            callbackOpen: function callbackOpen() {
                _callbackOpen();
            },
            callbackClose: function callbackClose() {
                _callbackClose();
            }
        });
        var modal2 = new Modal({
            el: "#modal",
            selector: ".js-modal2",
            color: "#7db122",
            content: "Повседневная практика показывает, что реализация намеченных плановых заданий представляет собой интересный эксперимент проверки соответствующий условий активизации. Задача организации, в особенности же укрепление и развитие структуры влечет за собой процесс внедрения и модернизации существенных финансовых и административных условий. Разнообразный и богатый опыт постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа направлений прогрессивного развития.",
            callbackOpen: function callbackOpen() {
                _callbackOpen();
            },
            callbackClose: function callbackClose() {
                _callbackClose();
            }
        });
        var modal3 = new Modal({
            el: "#modal",
            selector: ".js-modal3",
            color: "#f02b63",
            content: "Таким образом начало повседневной работы по формированию позиции позволяет оценить значение систем массового участия. С другой стороны консультация с широким активом способствует подготовки и реализации направлений прогрессивного развития. С другой стороны новая модель организационной деятельности в значительной степени обуславливает создание модели развития. Повседневная практика показывает, что сложившаяся структура организации играет важную роль в формировании позиций, занимаемых участниками в отношении поставленных задач. Задача организации, в особенности же начало повседневной работы по формированию позиции требуют от нас анализа систем массового участия. С другой стороны постоянный количественный рост и сфера нашей активности позволяет оценить значение направлений прогрессивного развития.",
            callbackOpen: function callbackOpen() {
                _callbackOpen();
            },
            callbackClose: function callbackClose() {
                _callbackClose();
            }
        });
    }
    function callbackToogleHide() {
        var $target = $("#callback-toggle");
        var $footer = $("#footer");
        var isInView = isElementInView($footer);
        if (win.innerWidth <= 767) {
            if (isInView) {
                $target.addClass("is-hidden");
            } else if (!isInView) {
                $target.removeClass("is-hidden");
            }
        } else {
            $target.removeClass("is-hidden");
        }
    }
    function productFilter() {
        $(".product-filter__item").click(function () {
            var el = $(this);
            var dropdown = $(".product-filter__item-dropdown");
            if (!el.hasClass("active")) {
                dropdown.removeClass("active");
                //dropdown.stop().slideUp(400);
                el.addClass("active");
                el.find(".product-filter__item-dropdown").stop().slideDown(400);
            } else {
                el.removeClass("active");
                el.find(".product-filter__item-dropdown").stop().slideUp(400);
            }
        });
    }
    doc.addEventListener("DOMContentLoaded", function () {
        modalInit();
        stickyNavbar.init();
        mobileMenu.init();
        callbackToogleHide();
        productFilter();
    });
    win.addEventListener("scroll", function () {
        callbackToogleHide();
    });
    $('a.js-scrollScreen[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[id=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 60
                }, 1000);
                return false;
            }
        }
    });
})(window, document);