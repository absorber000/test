var map = document.getElementById('contacts-map');
var panorama = document.getElementById('panorama');


    ymaps.ready(init);

    var myMap;
    var placemarkCentered = false;
    var initialized = false;

    function init() {
        myMap = new ymaps.Map("contacts-map", {
            center: [55.761261867034925,37.66121522919288],
            zoom: 17,
            controls: ['zoomControl']
        });
        myMap.behaviors.disable('scrollZoom');
        myMap.geoObjects
            .add(new ymaps.Placemark([55.7613053904511,37.66456579629513], {
                balloonContent: 'Нижний Сусальный пер., д.5, стр. 23, Москва, 105064'
            }, {
                iconLayout: 'default#image',
                iconImageHref: '/images/map-marker.svg',
                iconImageSize: [40, 57],
            }));
        inT();
        function inT(){
            if (window.innerWidth <= 959 && !placemarkCentered) {
                myMap.setBounds(myMap.geoObjects.getBounds(), {
                    checkZoomRange: true,
                    zoomMargin: 35
                });
                placemarkCentered = true;
            }
            if (window.innerWidth > 959 && placemarkCentered){
                myMap.setBounds([[55.76010613181842,37.66075641005371],[55.76215687764731,37.660928071430654]], {
                    checkZoomRange: true,
                    zoomMargin: 17
                });
                placemarkCentered = false;
            }
            if (window.innerWidth <= 768) {
                myMap.behaviors.disable('drag');
            }
        }
        window.addEventListener("resize", function() {
            myMap.container.fitToViewport();
            inT();
        });
    }




